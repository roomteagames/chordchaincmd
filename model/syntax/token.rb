class Token
  attr_reader :word, :character, :type

  # type -- :subject, :verb, :predicate

  def initialize(word, character, type)
  	@word = word
  	@character = character
  	@type = type
  end
end