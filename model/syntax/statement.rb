class Statement
  attr_reader :subject, :verb, :req_arguments, :opt_argument, :predicate_groups

  # req_arguments: array of required Argument objects
  # opt_argument: single optional argument object: occurs AFTER all required arguments are listed
  # predicate_groups: array of PredicateGroup objects used as modifiers

  def initialize(subject, verb, req_arguments, opt_argument, predicate_groups)
  	@subject = subject
  	@verb = verb
    @req_arguments = req_arguments
    @opt_argument = opt_argument
  	@predicate_groups = predicate_groups
  end
end