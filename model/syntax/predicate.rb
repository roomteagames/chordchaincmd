class Predicate
  attr_reader :token, :req_arguments, :opt_argument

  def initialize(token, req_arguments = [], opt_argument = nil, can_standalone: false)
    @token = token
    @req_arguments = req_arguments
    @opt_argument = opt_argument
    @can_standalone = can_standalone
  end

  def standalone?
  	return can_standalone
  end
end