class Argument
  attr_reader :type, :name, :default, :default_short

  # type - :fixed, :number, :string

  def initialize(type, name = nil, default: nil, default_short: nil)
  	@type = type
  	@name = name
  	@default = default
  	@default_short = default_short
  end
end