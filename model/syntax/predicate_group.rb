class PredicateGroup
  attr_accessor :predicates, :relationship

  # relationship - :all_optional, :exactly_one, :at_least_one, :at_most_one, :all_required

  def initialize(predicates, relationship = :all_optional)
    @predicates = predicates
    @relationship = relationship
  end
end