class Action
  attr_reader :obj_class, :type, :index, :params

  # obj_class -- class name of instance to modify (??? maybe)
  # type -- :read, :insert, :update, :delete
  # index -- index of where to find/insert this instance (for insert, leave as -1 to append to end)
  # params -- structure of params differs based on type
  #   read: list of read methods to run (i.e. print, etc)
  #   insert: list of arguments to pass in object ctor
  #   update: hash of field/new value pairs
  #   delete: {leave as empty}

  def initialize(obj_class, type, index: -1, params: {})
    @obj_class = obj_class
    @type = type
    @index = index
    @params = params
  end
end