class ChordType
  attr_accessor :type_name, :intervals

  def initialize(type_name, intervals)
    @type_name = type_name
    @intervals = intervals
  end
end