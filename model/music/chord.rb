class Chord
  attr_accessor :tonic, :type, :length

  def initialize(tonic, type)
    @tonic = tonic
    @type = type
    @length = length
  end
end