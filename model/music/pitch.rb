class Pitch
  attr_accessor :label, :octave

  def initialize(label, frequency)
    @label = label
    @pctave = octave
  end
end