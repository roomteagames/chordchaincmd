class Note
  attr_accessor :pitch, :length

  def initialize(pitch, length)
    @pitch = pitch
    @length = length
  end
end