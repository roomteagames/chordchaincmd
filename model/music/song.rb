class Song
  attr_accessor :name, :sections, :tempo, :time_signature, :length

  def __calculate_length
  	length = 0
    for s in @sections
      length = length + s.length
    end
    return length
  end

  def initialize(name, sections, tempo, time_signature)
  	@name = name
  	@sections = sections
  	@tempo = tempo
  	@time_signature = time_signature
  	@length = __calculate_length
  end
end