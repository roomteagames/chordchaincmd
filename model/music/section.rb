class Section
  attr_accessor :notes, :tempo, :time_signature, :length

  def __calculate_length
  	length = 0
    for n in @notes
      length = length + n.length
    end
    return length
  end

  def initialize(measures, tempo, time_signature)
    @measures = measures
    @tempo = tempo
    @time_signature = time_signature
    @length = __calculate_length
  end
end