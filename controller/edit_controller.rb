require_relative '../factory/syntax/labels'
require_relative '../factory/syntax/token_factory'
require_relative '../factory/syntax/predicate_factory'
require_relative '../factory/syntax/statement_factory'
require_relative '../service/translation_service'
require_relative '../service/song_service'

module EditController

  def self.execute(song)
  	# Initialize value tokens, predicates, and statements for input interpretation
	tokens = TokenFactory::get_token_hash
	predicates = PredicateFactory::get_predicate_hash(tokens[:predicates])
	statements = StatementFactory::get_statement_hash(tokens, predicates)
	translation_service = TranslationService.new(statements)
	song_service = SongService.new(song)

  	puts Labels::EDITING_HEADER song
    loop do
      print Labels::CMD_PROMPT
      command = gets.chomp
      break if command == 'quit' # in the future, turn this into a subjectless predicate
      action = translation_service.parse_statement(command)
      song_service.process_song_action(action)
    end
  end
end