class SongService
  attr_reader :song

  def initialize(song)
  	@song = song
  end

  def process_song_action(action)
  	# action defines something that needs to be done to/involving the song based on action (translated from command)
  end
end