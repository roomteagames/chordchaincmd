require_relative '../factory/syntax/labels'

class TranslationService
  attr_reader :statement_hash

  def initialize(statement_hash)
  	@statement_hash = statement_hash
  end

  def __process_subject(words)
    index = 0
    subject_term = words[index] # store subject for further usage
  	subject_hash = statement_hash[subject_term]
    if subject_hash == nil
      __process_predicate(words[index])
    else
      index = index + 1
      verb_term = words[index] # store verb for further usage
      statement = subject_hash[verb_term]
      index = index + 1

      #TODO: process arg(s) here for verb
      
      #State variables for loop
      statement_groups = statement.predicate_groups.clone
      current_group = nil
      current_group_predicates = []
      current_predicate = nil
      current_predicate_name = nil
      group_predicate_count = 0
      predicate_num_args = 0
      predicate_arg_count = 0

      until index == words.length
        word = words[index]
        new_group = statement_groups.reject! {|pg| pg.predicates[word]}[0] # get and simultaneously remove from list
        puts new_group

        #ERROR: invalid predicate used with verb
        if !current_group && !new_group
          raise Labels::ERR_INVALID_PRED(word, verb_term)
        end

        # Word represents a predicate in new group
        if new_group

          #ERROR: not enough arguments for current predicate
          if predicate_arg_count + 1 < predicate_num_args
            raise Labels::ERR_NOT_ENOUGH_ARGS(current_predicate_name, predicate_num_args, predicate_arg_count + 1)
          end

          current_group = new_group
          current_predicate = current_group.predicates.reject! {|k| k == word}[0]  # get and simultaneously remove from hash
          current_predicate_name = current_predicate.token.name
          group_predicate_count = 1
          predicate_num_args = current_predicate.req_arguments.length

          #TODO: actually convert this data into an action!

        # Word represents another predicate from current group
        elsif !new_group && current_group.predicates[word]

          #ERROR: not enough arguments for current predicate
          if predicate_arg_count + 1 < predicate_num_args
            raise Labels::ERR_NOT_ENOUGH_ARGS(current_predicate_name, predicate_num_args, predicate_arg_count + 1)
          end

          #TODO: actually convert this data into an action!

        # Word represents arg
        elsif !current_group_predicates[word]
          arg = current_predicate.req_arguments[predicate_arg_count]

          #ERROR: numeric arg is not correct argument type
          if arg.type == :number && (true if Integer(word) rescue false)
            raise Labels::ERR_NUM_ARG_WRONG_TYPE(current_predicate_name, predicate_arg_count, word)
          end

          #ERROR: fixed arg is not correct value
          if arg.type == :fixed && word != arg.default
            raise Labels::ERR_FIX_ARG_WRONG_VALUE(current_predicate_name, predicate_arg_count, arg.default, word)
          end

          arguments = arguments.push(word);
        end

        index += 1
      end

    end
  end

  def __process_statement_predicate(words, statement)

  end

  def __process_predicate(words)

  end

  def parse_statement(text)
    words = text.split(' ')
    p words
    
    __process_subject(words)
    return nil
  end

end