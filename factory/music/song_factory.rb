require_relative '../../model/music/song'

module SongFactory

  def self.__load(filename)
    # implement file parser to reconstruct song
    return Song.new(filename, [], 120, '4/4')
  end  

  def self.get_song(filename)
  	song = nil
    if File.exist?(filename)
      song = __load(filename)
    else
      song = Song.new(filename, [], 120, '4/4')
    end

    return song
  end

end