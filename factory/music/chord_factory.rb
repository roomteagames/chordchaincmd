require_relative '../../model/music/chord_type'
require_relative '../../model/music/chord'

module ChordFactory
  def self.create_major(tonic)
    return Chord.new(
      tonic,
      ChordType.new('major', [2, 1.5])
    )
  end

  def self.create_minor(tonic)
    return Chord.new(
      tonic,
      ChordType.new('major', [1.5, 2])
    )
  end

  def self.create_diminished(tonic)
    return Chord.new(
      tonic,
      ChordType.new('major', [1.5, 1.5])
    )
  end

  def self.create_augmented(tonic)
    return Chord.new(
      tonic,
      ChordType.new('major', [2, 2])
    )
  end
end