require_relative '../../model/syntax/argument'
require_relative '../../model/syntax/predicate_group'
require_relative '../../model/syntax/statement'
require_relative '../../model/syntax/token'
require_relative 'keywords'

module StatementFactory

  def self.__get_section_statements(token_hash, predicate_hash)
	subject_hash = token_hash[:subjects]
	verb_hash = token_hash[:verbs]

	return {
	  Keywords::EDIT => Statement.new(
	    subject_hash[Keywords::SECTION],
	    verb_hash[Keywords::EDIT],
		[],
		Argument.new(:number, Keywords::ARG_ID),
	    [
	      PredicateGroup.new(
	      	{Keywords::BEFORE => predicate_hash[Keywords::BEFORE], Keywords::AFTER => predicate_hash[Keywords::AFTER]},
	      	:at_most_one
	      ),
	      PredicateGroup.new(
	      	{Keywords::LENGTH => predicate_hash[Keywords::LENGTH]}
	      ),
	      PredicateGroup.new(
	      	{Keywords::TEMPO => predicate_hash[Keywords::TEMPO]}
	      ),
	      PredicateGroup.new(
	      	{Keywords::PLAY => predicate_hash[Keywords::PLAY]}
	      ),
	      PredicateGroup.new(
	      	{Keywords::SHOW => predicate_hash[Keywords::SHOW]}
	      )
	    ]
	  ),
	  Keywords::DELETE => Statement.new(
	    subject_hash[Keywords::SECTION],
	    verb_hash[Keywords::DELETE],
		[Argument.new(:number, Keywords::ARG_ID)],
		nil,
	    [
	      PredicateGroup.new(
	      	{Keywords::SHOW => predicate_hash[Keywords::SHOW]}
	      )
	    ]
	  ),
	}
  end

	def self.__get_note_statements(token_hash, predicate_hash)
	  subject_hash = token_hash[:subjects]
	  verb_hash = token_hash[:verbs]

	  return {
	    Keywords::EDIT => Statement.new(
	      subject_hash[Keywords::NOTE],
	      verb_hash[Keywords::EDIT],
		  [],
		  Argument.new(:number, Keywords::ARG_ID),
	      [
	      	PredicateGroup.new(
	      	  {Keywords::VALUE => predicate_hash[Keywords::VALUE]}
	      	),
	      	PredicateGroup.new(
	      	  {Keywords::BEFORE => predicate_hash[Keywords::BEFORE], Keywords::AFTER => predicate_hash[Keywords::AFTER]},
	      	  :at_most_one
	      	),
	      	PredicateGroup.new(
	      	  {Keywords::LENGTH => predicate_hash[Keywords::LENGTH]}
	      	),
	      	PredicateGroup.new(
	      	  {Keywords::TEMPO => predicate_hash[Keywords::TEMPO]}
	      	),
	      	PredicateGroup.new(
	      	  {Keywords::PLAY => predicate_hash[Keywords::PLAY]}
	      	),
	      	PredicateGroup.new(
	      	  {Keywords::SHOW => predicate_hash[Keywords::SHOW]}
	      	)
	      ]
	    ),
	    Keywords::DELETE => Statement.new(
	      subject_hash[Keywords::NOTE],
	      verb_hash[Keywords::DELETE],
		  [Argument.new(:number, Keywords::ARG_ID)],
		  nil,
	      [
	      	PredicateGroup.new(
	      	  {Keywords::SHOW => predicate_hash[Keywords::SHOW]}
	      	)
	      ]
	    ),
	  }
	end

	def self.get_statement_hash(token_hash, predicate_hash)
	  return {
        Keywords::SECTION => __get_section_statements(token_hash, predicate_hash),
        Keywords::NOTE => __get_note_statements(token_hash, predicate_hash),
        # TODO: add more here!
	  }
	end
end