module Keywords
  
  # Subject Keywords
  SECTION = 'section'
  SECTION_SHORT = 's'
  NOTE = 'note'
  NOTE_SHORT = 'n'
  CHORD = 'chord'
  CHORD_SHORT = 'c'

  # Verb Keywords
  EDIT = 'edit'
  EDIT_SHORT = 'e'
  DELETE = 'delete'
  DELETE_SHORT = 'd'
  INSERT = 'insert'
  INSERT_SHORT = 'i'

  # Predicate Keywords
  SHOW = 'show'
  SHOW_SHORT = 's'
  PLAY = 'play'
  PLAY_SHORT = 'p'
  VALUE = 'value'
  VALUE_SHORT = 'v'
  TEMPO = 'tempo'
  TEMPO_SHORT = 't'
  TIME_SIGNATURE = 'time-signature'
  TIME_SIGNATURE_SHORT = 'ts'
  LENGTH = 'length',
  LENGTH_SHORT = 'l'
  BEFORE = 'before'
  BEFORE_SHORT = 'b'
  AFTER = 'after'
  AFTER_SHORT = 'a'

  # Argument Keywords
  ARG_EDITS = 'edits'
  ARG_EDITS_SHORT = 'e'
  ARG_ID = 'id'

end
