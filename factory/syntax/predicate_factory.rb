require_relative '../../model/syntax/argument'
require_relative '../../model/syntax/predicate'
require_relative 'keywords'

module PredicateFactory
  
  def self.get_predicate_hash(predicate_token_hash)
    return {
      Keywords::SHOW => Predicate.new(
      	predicate_token_hash[Keywords::SHOW],
		[],
		Argument.new(:fixed, default: Keywords::ARG_EDITS, default_short: Keywords::ARG_EDITS_SHORT),  
      	can_standalone: true
      ),
      Keywords::PLAY => Predicate.new(
      	predicate_token_hash[Keywords::PLAY],
		[],
		nil,
		can_standalone: true
      ),
      Keywords::VALUE => Predicate.new(
      	predicate_token_hash[Keywords::VALUE],
      	[Argument.new(:string)]
      ),
      Keywords::TEMPO => Predicate.new(
      	predicate_token_hash[Keywords::TEMPO],
		[Argument.new(:number)],
		nil,
		can_standalone: true
      ),
      Keywords::TIME_SIGNATURE => Predicate.new(
      	predicate_token_hash[Keywords::TIME_SIGNATURE],
		[Argument.new(:string, default: '4/4')],
		nil,  
		can_standalone: true
      ),
      Keywords::LENGTH => Predicate.new(
      	predicate_token_hash[Keywords::LENGTH],
		[Argument.new(:number, default: 1)],
		nil,
		can_standalone: true
      ),
      Keywords::BEFORE => Predicate.new(
      	predicate_token_hash[Keywords::BEFORE],
		[Argument.new(:number)]
      ),
      Keywords::AFTER => Predicate.new(
      	predicate_token_hash[Keywords::AFTER],
		[Argument.new(:number)]
      ),
    }
  end
end