require_relative '../../model/syntax/token'
require_relative 'keywords'

module TokenFactory
  def self.__get_subject_hash
    return {
      Keywords::SECTION => Token.new(
        Keywords::SECTION,
        Keywords::SECTION_SHORT,
        :subject
      ),
      Keywords::NOTE => Token.new(
        Keywords::NOTE,
        Keywords::NOTE_SHORT,
        :subject
      ),
      Keywords::CHORD => Token.new(
        Keywords::CHORD,
        Keywords::CHORD_SHORT,
        :subject
      ),
    }
  end

  def self.__get_verb_hash
    return {
      Keywords::EDIT => Token.new(
        Keywords::EDIT,
        Keywords::EDIT_SHORT,
        :verb
      ),
      Keywords::DELETE => Token.new(
        Keywords::DELETE,
        Keywords::DELETE_SHORT,
        :verb
      ),
      Keywords::INSERT => Token.new(
        Keywords::INSERT,
        Keywords::INSERT_SHORT,
        :verb
      ),
    }
  end

  def self.__get_predicate_hash
    return {
      Keywords::SHOW => Token.new(
        Keywords::SHOW,
        Keywords::SHOW_SHORT,
        :predicate
      ),
      Keywords::PLAY => Token.new(
        Keywords::PLAY,
        Keywords::PLAY_SHORT,
        :predicate
      ),
      Keywords::VALUE => Token.new(
        Keywords::VALUE,
        Keywords::VALUE_SHORT,
        :predicate
      ),
      Keywords::TEMPO => Token.new(
        Keywords::TEMPO,
        Keywords::TEMPO_SHORT,
        :predicate
      ),
      Keywords::TIME_SIGNATURE => Token.new(
        Keywords::TIME_SIGNATURE,
        Keywords::TIME_SIGNATURE_SHORT,
        :predicate
      ),
      Keywords::LENGTH => Token.new(
        Keywords::LENGTH,
        Keywords::LENGTH_SHORT,
        :predicate
      ),
      Keywords::BEFORE => Token.new(
        Keywords::BEFORE,
        Keywords::BEFORE_SHORT,
        :predicate
      ),
      Keywords::AFTER => Token.new(
        Keywords::AFTER,
        Keywords::AFTER_SHORT,
        :predicate
      ),
    }
  end

  def self.get_token_hash
  	return {
  	  :subjects => __get_subject_hash,
  	  :verbs => __get_verb_hash,
  	  :predicates => __get_predicate_hash,
  	}
  end
end