module Labels
  
  # General labels / prompts
  TITLE_GREETING = "================================\n-----Welcome to ChordChain------\n================================"
  CMD_PROMPT = "--> "

  # Headers
  INF_HDR_EDIT = 'EDITING: '
  ERR_HDR_GRAMMAR = 'GRAMMAR ERROR: '

  # Dynamic labels
  def self.EDITING_HEADER(song)
    return "#{INF_HDR_EDIT}'#{song.name}'"
  end

  def self.ERR_INVALID_PRED(name, verb)
    return "#{ERR_HDR_GRAMMAR}Invalid predicate '#{name}' used with verb '#{verb}'"
  end

  def self.ERR_NOT_ENOUGH_ARGS(name, num_req_args, arg_index)
    return "#{ERR_HDR_GRAMMAR}Predicate '#{name}' requires #{num_req_args}; only #{arg_index} given..."
  end

  def self.ERR_NUM_ARG_WRONG_TYPE(name, arg_index, value)
    return "#{ERR_HDR_GRAMMAR}Argument #{arg_index} for predicate '#{name}' is numeric, but given value '#{value}'..."
  end

  def self.ERR_FIX_ARG_WRONG_VALUE(name, arg_index, expected, given)
    return "#{ERR_HDR_GRAMMAR}Argument #{arg_index} for predicate '#{name}' is fixed with expected value '#{expected}', but given '#{given}'..."
  end
end